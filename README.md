# shellApp for ITk PDB

### [Streamlit](https://www.streamlit.io) (python**3**) code to upload component/test schemas for registration on production database (PDB)

**NB** now using [itkdb](https://pypi.org/project/itkdb/)

Check requirements file for necessary libraries

---

## Running

*Stand Alone* version:

Get libraries:
> python3 -m pip install -r requirements

Run WebApp:
> streamlit run shellApp.py
  * option to add credentials at end with format: ''ac1:X ac2:Y''

<!-- **Docker version**
[*Docker version*](https://hub.docker.com/repository/docker/kwraight/streamlit_pdb_uploader):
Run WebApp:
> docker run -p 8501:8501 kwraight/streamlit_pdb_uploader:TAG_ID streamlit run code/uploaderApp.py -->

---

## WebApp Layout

**Basic Idea:**
* interact with ITk production database (PDB)


### Top Page
  * input PDB access codes (only stored in browser cache)


### Debug Page:
**(Broom cupboard)**
  * State settings

---

## Some explanatory notes

Basic code structure:
  * shellApp: call pages, set sidebar  
  * top page: get user details
  * debug page: check streamlit state information
  * stInfrastructure: state class definition
  * requirements: library versions
  * README: *you are here*

Explaining the component files...

### stInfrastructure
Contains useful streamlit functionality.

At the top, some useful functions for displaying widgets and download table information.
Below, session state definition: this defines an object to hold per-session information and so avoid repeated user input.

### requirements
Required libraries for App.
Can automatically generate file using [pipreqs](https://pypi.org/project/pipreqs/).
**NB** Check versions in file. *streamlit* version seems to get stuck at *0.61.0*, should be *>=0.71.0*.

### shellApp
Main file for running *streamlit*.

At the top, some useful functions for obtaining PDB access token.
Below, main part includes set-up sidebar including what pages can be selected and a debug toggle.

### page_top
Opening page for inputing user info. to get PDB token.

At the top, some useful functions for obtaining PDB access token.
Below, main part setting out input widgets and check commandline arguments for optional credentials.

### page_debug
Check state variable for debugging purposes.

At the top, some useful functions displaying info.
Below, main part setting out debug information (*i.e.* state information) with clear option.
