import streamlit as st
import datetime
import os
import sys
import json
import subprocess
import ast
### PDB stuff
import itkdb

#####################
### useful functions
#####################

def AuthenticateUser(ac1,ac2):
    user = itkdb.core.User(accessCode1=ac1, accessCode2=ac2)
    user.authenticate()
    client = itkdb.Client(user=user)
    return client

### format datetime
def DateFormat(dt):
    return str("{0:02}-{1:02}-{2:04}".format(dt.day,dt.month,dt.year))+" at "+str("{0:02}:{1:02}".format(dt.hour,dt.minute))


def GetCodes(args):
    code1,code2=None,None
    for a in args:
        if "ac1" in a: code1=a[4::].strip('"').strip("'")
        if "ac2" in a: code2=a[4::].strip('"').strip("'")
    return code1,code2

#####################
### top page
#####################

### main part
def main_part(state):
    nowTime = datetime.datetime.now()
    st.write("""## :fireworks: **Get Token** :fireworks: """)
    st.write("""### :calendar: ("""+DateFormat(nowTime)+""")""")
    st.write(" --- ")
    ###

    # debug check
    if state.debug:
        st.write("### Debug is on")

    # token check
    try:
        if state.myToken:
            st.write(":white_check_mark: Got Token")
        else:
            st.write("No user found")
    except AttributeError:
        st.write("No user state set form commandline")

    if state.debug: st.write("sys args:",sys.argv)
    if len(sys.argv)>1:
        ac1,ac2=GetCodes(sys.argv)
        st.info("Read codes from commandLine")
        #st.write(ac1,",",ac2)
        if ac1==None or ac2==None:
            st.error("Problem recognising credentials. Please check arguments and try again.")
        else:
            state.myTime=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
            state.ac1,state.ac2=ac1,ac2
            state.myClient=AuthenticateUser(state.ac1,state.ac2)
    else:
        # input passwords
        try:
            state.ac1 = st.text_input(label='Enter password 1', type="password", value=state.ac1)
        except AttributeError:
            state.ac1 = st.text_input(label='Enter password 1', type="password")
        try:
            state.ac2 = st.text_input(label='Enter password 2', type="password", value=state.ac2)
        except AttributeError:
            state.ac2 = st.text_input(label='Enter password 2', type="password")

        if state.debug:
            st.write("**DEBUG** tokens")
            st.write("ac1:",state.ac1,", ac2:",state.ac2)

        if st.button("Get Token"):
            state.myTime=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
            state.myClient=AuthenticateUser(state.ac1,state.ac2)

    try:
        st.write("Registed at",state.myTime)
        exTime = datetime.datetime.fromtimestamp(state.myClient.user.expires_at)
        st.write("Token expires at: "+exTime.strftime("%d.%m.%Y %H:%M"))

        # feedback on passwords
        try:
            state.user=state.myClient.get('getUser', json={'userIdentity': state.myClient.user.identity})
            st.success("Returning token for "+str(state.user['firstName'])+" "+str(state.user['lastName'])+" seems ok. (check debug for details)")
            if state.debug:
                st.write("User (id):",state.user['firstName'],state.user['lastName'],"("+state.user['id']+")")
        except:
            st.error("Bad token registered. Please close streamlit and try again")
    except AttributeError:
        st.info("Not token yet registed")
