import streamlit as st
###

#####################
### useful functions
#####################

def display_state_values(state):

    st.write("## All data")
    st.write("Debug setting:", state.debug)
    st.write("---")

    ### token
    try:
        if state.myClient:
            st.write(":white_check_mark: Got token")
            try:
                st.write("Token issued:",state.myTime)
            except AttributeError:
                pass
        else:
            st.write("No token found")
    except AttributeError:
        st.write("No token defined")
    ### User ID
    st.write("### User Info.")
    try:
        st.write("User set:",state.user['firstName'],state.user['middleName'],state.user['lastName'])
        try:
            #st.write(state.toggleToken)
            if state.toggleToken:
                st.write(state.user)
            if st.button("Details"):
                state.toggleToken= not state.toggleToken
        except AttributeError:
            state.toggleToken=False
    except AttributeError:
        st.write("User not yet set")

    if state.debug:
        st.write("### All Info.")
        for k,v in vars(state).items():
            st.write(k,":",v)


#####################
### debug page
#####################

def main_part(state):
    st.title(":wrench: Broom cupboard")
    st.write("---")
    st.write("## Bits and bobs for maintainance")
    st.write("---")

    display_state_values(state)

    st.write("## :exclamation: Clear all state settings")
    if st.button("Clear state"):
        try:
            del state.ids
            del state.myClient
            del state.myTime
            del state.user
        except AttributeError:
            st.write("No state info. defined yet")
