### streamlit stuff
import streamlit as st
### general stuff
import os
import sys
from datetime import datetime
###session stuff
cwd = os.getcwd()
sys.path.insert(1, cwd)
import stInfrastructure as infra
import itkdb
### pages
import page_top
import page_debug

#####################
### useful functions
#####################

def AuthenticateUser(ac1,ac2):
    user = itkdb.core.User(accessCode1=ac1, accessCode2=ac2)
    user.authenticate()
    client = itkdb.Client(user=user)
    return client

#####################
### main
#####################

def main():
    ### get state variable
    state = infra.get()

    ### define pages dictionary
    pages = {
        "Top Page": page_top.main_part,
        "Broom cupboard": page_debug.main_part,
    }

    ### sidebar
    st.sidebar.title(":telescope: My WebApp")
    st.sidebar.markdown("---")
    page = st.sidebar.radio("Select your page:", tuple(pages.keys()))

    ### mini-state summary
    if st.sidebar.button("State Summary"):
        ### token
        try:
            if len(state.myToken)>0:
                st.sidebar.markdown("Got token")
            else:
                st.sidebar.markdown("No token found")
        except AttributeError:
            st.sidebar.markdown("No token defined")
        ### component IDs
        try:
            st.sidebar.markdown("Component IDs defined: "+str(len(state.ids)))
        except AttributeError:
            st.sidebar.markdown("No components defined")

    ##############
    # renew button on sidebar
    ##############
    try:
        if state.myClient:
            exTime = datetime.fromtimestamp(state.myClient.user.expires_at)
            st.sidebar.markdown("Token expires at: "+exTime.strftime("%d.%m.%Y %H:%M"))
        if state.ac1 and state.ac2:
            if st.sidebar.button("Renew Token"):
                nowTime = datetime.now()
                state.myTime=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
                st.sidebar.markdown("Registed at: "+state.myTime)
                state.myClient=AuthenticateUser(state.ac1,state.ac2)
        else:
            st.sidebar.markdown("register on top page")
        #state.myClient=AuthenticateUser(state.ac1,state.ac2)
    except:
        st.sidebar.markdown("No client set")

    ### debug toggle
    st.sidebar.markdown("---")
    debug = st.sidebar.checkbox("Toggle debug")
    if debug:
        state.debug=True
    else: state.debug=False

    ### display  selected page using state variable
    pages[page](state)


#########################################################################################################

#########################################################################################################

### run
if __name__ == "__main__":
    main()
